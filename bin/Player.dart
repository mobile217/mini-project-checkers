class Player{
var symbol;
  int win=0;

  Player(var symbol){
    this.symbol = symbol;
  }
  
  String getSymbol(){
    return symbol;
  }

  int getWin(){
    return win;
  }

  void Win(){
    this.win++;
  }

  @override
  String toString() {
    return "Player : "+ symbol + "   Win = " + win.toString();
  }
}