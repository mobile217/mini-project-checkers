import 'dart:io';

import 'Game.dart';

void main(List<String> arguments) {
  Game game = new Game();
  int count = 0;
  game.newboard();
  game.clearScreen();
  while (true) {
    game.clearScreen();
    game.ShowScore();
    game.showTable();
    game.showTurn();
    if (game.input1()) {
      game.clearScreen();
      game.ShowScore();
      game.showTable();
      game.showTurn();
      if (game.input2()) {
        count++;
        if (game.count > 0) {
          while (game.chackEatAgain()) {
            game.clearScreen();
            game.ShowScore();
            game.showTable();
            game.showTurn();
            while (game.input3()) {}
          }
        }
      }
      if (count > 0) {
        game.switPlayer();
        count = 0;
      }
    }
    game.setKing();
    game.count = 0;
    game.clearScreen();
    game.ShowScore();
    game.showTable();
    if(game.checkWin()){
      game.newboard();
      game.clearScreen();
    }
  }
}
