import 'dart:io';

import 'Board.dart';
import 'Player.dart';

class Game {
  late Player R;
  late Player B;
  var col1 = -1;
  var row1 = -1;
  var col2 = -1;
  var row2 = -1;
  var rcol = -1; //ขวา
  var rrow = -1;
  var lcol = -1; //ซ้าย
  var lrow = -1;
  var rcolking = -1; //ขวา
  var rrowking = -1;
  var lcolkign = -1; //ซ้าย
  var lrowking = -1;
  late Board board;
  int count = 0;

  Game(){
    this.R = new Player('R');
    this.B = new Player('B');
  }

  newboard() { // สร้าง board ใหม่
    this.board = new Board(R,B);
  }

  clearScreen() { // เคลียร์หน้าจอ
    print("\x1B[2J\x1B[0;0H");
  }

  showTable() { // แสดงตาราง
    var table = board.getTable();
    print("    1   2   3   4   5   6   7   8");
    for (int r = 0; r < table.length; r++) {
      print("  +---+---+---+---+---+---+---+---+");
      for (int c = 0; c < table[r].length; c++) {
        if (c == 0) stdout.write("" + (r + 1).toString() + " "); // แสดงตัวเลขด้านซ้าย
        stdout.write("|");

        if ((r == lrow && c == lcol) || (r == rrow && c == rcol) || (r == lrowking && c == lcolkign) || (r == rrowking && c == rcolking)) {
          stdout.write("\u001b[42;1m " + table[r][c] + " \u001b[0m"); // แสดงตำแหน่งที่ตัวหมากสามารถเดินได้แบบมีสี
        } else {
          playerColor(table[r][c]); // ใส่สีตัวหมาก
          stdout.write(" " + table[r][c] + " "); // แสดงตัวหมาก
          resetColor();
        }
        if (c == table[r].length - 1) { 
          stdout.write("|");
          stdout.write(" " + (r + 1).toString() + ""); // แสดงตัวเลขด้านขวา
        }
      }
      print("");
    }
    print("  +---+---+---+---+---+---+---+---+");
    print("    1   2   3   4   5   6   7   8");
  }

  showTurn() { // แสดง turn ของตัวหมาก
    stdout.write("\nTurn : ");
    playerColor(board.currentPlayer);
    stdout.write(board.currentPlayer);
    print('');
    resetColor();
  }

  input1() {
     // รับค่าตำแหน่งตัวหมาก
    var table = board.getTable();
    stdout.write("Please Input col : ");
    int? col1 = int.parse(stdin.readLineSync()!); // รับค่า col1 แนวนอน
    stdout.write("Please Input row : ");
    int? row1 = int.parse(stdin.readLineSync()!); // รับค่า row1 แนวตั้ง
    if (col1 != 0 || row1 != 0) { // ตรวจสอบค่าที่รับไม่เท่ากับ 0
      if (row1 > 0 &&
          row1 < 9 &&
          col1 > 0 &&
          col1 < 9) { // ตรวจสอบขอบตาราง 1-8
        this.row1 = row1;
        this.col1 = col1;
        int count = 0;
        if (table[row1 - 1][col1 - 1] == '♟' && board.currentPlayer == '♟') { // ตรวจสอบตัวหมากฝั่งสีแดง
        // ตรวจสอบศัตรูบนซ้าย
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][(col1 - 2)] ==
                          '♙' ||
                      table[(row1 - 2)][(col1 - 2)] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 - 3)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูบนขวา
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][col1] == '♙' ||
                      table[(row1 - 2)][col1] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านบนซ้าย
            if (count == 0 &&
                table[(row1 - 2)][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcol = col1 - 2;
              this.lrow = row1 - 2;
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านบนขวา
            if (count == 0 &&
                table[(row1 - 2)][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcol = col1;
              this.rrow = row1 - 2;
            }
          }
        } else if (table[(row1 - 1)][(col1 - 1)] == '♙' && board.currentPlayer == '♙') { // ตรวจสอบตัวหมากฝั่งสีน้ำเงิน
        // ตรวจสอบศัตรูล่างซ้าย
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][(col1 - 2)] == '♟' ||
                      table[row1][(col1 - 2)] == '♛') &&
                  table[(row1 + 1)][(col1 - 3)] == ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างขวา
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][col1] == '♟' ||
                      table[row1][col1] == '♛') &&
                  table[(row1 + 1)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านล่างซ้าย
            if (count == 0 &&
                table[row1][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcol = col1 - 2;
              this.lrow = row1;
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านล่างขวา
            if (count == 0 &&
                table[row1][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcol = col1;
              this.rrow = row1;
            }
          }
        } else if (table[(row1 - 1)][(col1 - 1)] == '♛' && board.currentPlayer == '♟') { // ตรวจสอบตัวฮอสฝั่งสีแดง
          // ตรวจสอบศัตรูบนซ้าย
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][(col1 - 2)] ==
                          '♙' ||
                      table[(row1 - 2)][(col1 - 2)] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 - 3)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูบนขวา
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][col1] == '♙' ||
                      table[(row1 - 2)][col1] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างซ้าย
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][(col1 - 2)] == '♙' ||
                      table[row1][(col1 - 2)] == '♕') &&
                  table[(row1 + 1)][(col1 - 3)] == ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.lcolkign = col1 - 3;
                this.lrowking = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างขวา
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][col1] == '♙' ||
                      table[row1][col1] == '♕') &&
                  table[(row1 + 1)][(col1 + 1)] ==
                      ' ') {  // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.rcolking = col1 + 1;
                this.rrowking = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านบนซ้าย
            if (count == 0 &&
                table[(row1 - 2)][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcol = col1 - 2;
              this.lrow = row1 - 2;
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านบนขวา
            if (count == 0 &&
                table[(row1 - 2)][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcol = col1;
              this.rrow = row1 - 2;
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านล่างซ้าย
            if (count == 0 &&
                table[row1][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcolkign = col1 - 2;
              this.lrowking = row1;
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านล่างขวา
            if (count == 0 &&
                table[row1][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcolking = col1;
              this.rrowking = row1;
            }
          }
        } else if (table[(row1 - 1)][(col1 - 1)] == '♕' && board.currentPlayer == '♙') { // ตรวจสอบตัวฮอสฝั่งสีน้ำเงิน
          // ตรวจสอบศัตรูบนซ้าย
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][(col1 - 2)] ==
                          '♟' ||
                      table[(row1 - 2)][(col1 - 2)] ==
                          '♛') &&
                  table[(row1 - 3)][(col1 - 3)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูบนขวา
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][col1] == '♟' ||
                      table[(row1 - 2)][col1] ==
                          '♛') &&
                  table[(row1 - 3)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 - 3;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างซ้าย
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][(col1 - 2)] == '♟' ||
                      table[row1][(col1 - 2)] == '♛') &&
                  table[(row1 + 1)][(col1 - 3)] == ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.lcolkign = col1 - 3;
                this.lrowking = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างขวา
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][col1] == '♟' ||
                      table[row1][col1] == '♛') &&
                  table[(row1 + 1)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.rcolking = col1 + 1;
                this.rrowking = row1 + 1;
                board.eat = 1;
                count++;
              }
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านบนซ้าย
            if (count == 0 &&
                table[(row1 - 2)][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcol = col1 - 2;
              this.lrow = row1 - 2;
            }
          }
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านบนขวา
            if (count == 0 &&
                table[(row1 - 2)][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcol = col1;
              this.rrow = row1 - 2;
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบช่องการเดินทางด้านล่างซ้าย
            if (count == 0 &&
                table[row1][(col1 - 2)] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.lcolkign = col1 - 2;
              this.lrowking = row1;
            }
          }
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบช่องการเดินทางด้านล่างขวา
            if (count == 0 &&
                table[row1][col1] == ' ') { // ตรวจสอบตำแหน่งที่จะเดินว่างไหม
              this.rcolking = col1;
              this.rrowking = row1;
            }
          } 
        }

        count = 0;
        if (rcol == -1 && rrow == -1 && lcol == -1 && lrow == -1 && rcolking == -1 && rrowking == -1 && lcolkign == -1 && lrowking == -1) { 
          return false;
        }
        return true;
      }
    }
    print("Error");
    sleep(const Duration(seconds: 1));
    return false;
  }

  input2() {
    // รับค่าตำแหน่งที่จะเดินตัวหมาก
    stdout.write("Go to col : ");
    int? col2 = int.parse(stdin.readLineSync()!); // รับค่า col2 แนวนอน
    stdout.write("Go to row : ");
    int? row2 = int.parse(stdin.readLineSync()!); // รับค่า row2 แนวตั้ง
    this.row2 = row2;
    this.col2 = col2;
    if ((col2 != 0 && row2 != 0)) { // ตรวจสอบค่าที่รับเข้ามา เท่ากับ 0 หรือไม่
      if ((col2 == (lcol + 1) && row2 == (lrow + 1)) ||
          (col2 == (rcol + 1) && row2 == (rrow + 1))||
          (col2 == (rcolking + 1) && row2 == (rrowking + 1))||
          (col2 == (lcolkign + 1) && row2 == (lrowking + 1))) {
            if(board.eat > 0){
              count++;
            }
        board.setTable(row1, col1, row2, col2);
        col1 = -1;
        row1 = -1;
        this.lcol = -1;
        this.lrow = -1;
        this.rcol = -1;
        this.rrow = -1;
        this.lcolkign = -1;
        this.lrowking = -1;
        this.rcolking = -1;
        this.rrowking = -1;
        return true;
      } else {
        print("Error");
        sleep(const Duration(seconds: 1));
      }
    }
        this.lcol = -1;
        this.lrow = -1;
        this.rcol = -1;
        this.rrow = -1;
        this.lcolkign = -1;
        this.lrowking = -1;
        this.rcolking = -1;
        this.rrowking = -1;

    return false;
  }

  input3() {
    // รับค่าตำแหน่งที่จะเดินตัวหมาก
    stdout.write("Go to col : ");
    int? col2 = int.parse(stdin.readLineSync()!); // รับค่า col2 แนวนอน
    stdout.write("Go to row : ");
    int? row2 = int.parse(stdin.readLineSync()!); // รับค่า row2 แนวตั้ง
    this.row2 = row2;
    this.col2 = col2;
    if ((col2 != 0 && row2 != 0)) { // ตรวจสอบค่าที่รับเข้ามา เท่ากับ 0 หรือไม่
      if ((col2 == (lcol + 1) && row2 == (lrow + 1)) ||
          (col2 == (rcol + 1) && row2 == (rrow + 1))||
          (col2 == (rcolking + 1) && row2 == (rrowking + 1))||
          (col2 == (lcolkign + 1) && row2 == (lrowking + 1))) {
        board.setTable(row1, col1, row2, col2);
        col1 = -1;
        row1 = -1;
        this.lcol = -1;
        this.lrow = -1;
        this.rcol = -1;
        this.rrow = -1;
        this.lcolkign = -1;
        this.lrowking = -1;
        this.rcolking = -1;
        this.rrowking = -1;
        return false;
      } else {
        print("Error");
        sleep(const Duration(seconds: 1));
      }
    }
    return true;
  }

  switPlayer() { // สลับผู้เล่น
    board.switchPlayer();
  }

  chackEatAgain() { // ตรวจสอบการกินหลายต่อ
    int countEatAgain = 0;
    this.row1 = row2;
    this.col1 = col2;
    var table = board.getTable();
    if (table[(row1 - 1)][(col1 - 1)] == '♟') { // ตรวจสอบตัวหมากฝั่งสีแดง
    // ตรวจสอบศัตรูบนซ้าย
      if ((row2 - 2) > -1 &&
          (row2 - 2) < 8 &&
          (col2 - 2) > -1 &&
          (col2 - 2) < 8) { // ตรวจสอบขอบตาราง
        if ((row2 - 3) > -1 &&
            (row2 - 3) < 8 &&
            (col2 - 3) > -1 &&
            (col2 - 3) < 8) { // ตรวจสอบขอบตาราง
          if (table[(row2 - 2)][(col2 - 2)] == '♙' &&
              table[(row2 - 3)][(col2 - 3)] == ' ') {  // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
            this.lcol = col2 - 3;
            this.lrow = row2 - 3;
            board.eat = 1;
            countEatAgain++;
          }
        }
      }
      // ตรวจสอบศัตรูบนขวา
      if ((row2 - 2) > -1 &&
          (row2 - 2) < 8 &&
          col2 > -1 &&
          col2 < 8) { // ตรวจสอบขอบตาราง
        if ((row2 - 3) > -1 &&
            (row2 - 3) < 8 &&
            (col2 + 1) > -1 &&
            (col2 + 1) < 8) { // ตรวจสอบขอบตาราง
          if (table[(row2 - 2)][col2] == '♙' &&
              table[(row2 - 3)][(col2 + 1)] == ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
            this.rcol = col2 + 1;
            this.rrow = row2 - 3;
            board.eat = 1;
            countEatAgain++;
          }
        }
      }
    } else if (table[(row1 - 1)][(col1 - 1)] == '♙') { // ตรวจสอบตัวหมากฝั่งสีน้ำเงิน
      // ตรวจสอบศัตรูล่างซ้าย
      if (row2 > -1 &&
          row2 < 8 &&
          (col2 - 2) > -1 &&
          (col2 - 2) < 8) { // ตรวจสอบขอบตาราง
        if ((row2 + 1) > -1 &&
            (row2 + 1) < 8 &&
            (col2 - 3) > -1 &&
            (col2 - 3) < 8) { // ตรวจสอบขอบตาราง
          if (table[row2][(col2 - 2)] == '♟' &&
              table[(row2 + 1)][(col2 - 3)] == ' ') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
            this.lcol = col2 - 3;
            this.lrow = row2 + 1;
            board.eat = 1;
            countEatAgain++;
          }
        }
      }
      // ตรวจสอบศัตรูล่างขวา
      if (row2 > -1 &&
          row2 < 8 &&
          col2 > -1 &&
          col2 < 8) { // ตรวจสอบขอบตาราง
        if ((row2 + 1) > -1 &&
            (row2 + 1) < 8 &&
            (col2 + 1) > -1 &&
            (col2 + 1) < 8) { // ตรวจสอบขอบตาราง
          if (table[row2][col2] == '♟' &&
              table[(row2 + 1)][(col2 + 1)] == ' ' &&
              table[row2][col2] != '♙') { // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
            this.rcol = col2 + 1;
            this.rrow = row2 + 1;
            board.eat = 1;
            countEatAgain++;
          }
        }
      }
    }else if(table[(row1 - 1)][(col1 - 1)] == '♛'){ // ตรวจสอบตัวฮอสฝั่งสีแดง
      // ตรวจสอบศัตรูบนซ้าย
      if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][(col1 - 2)] ==
                          '♙' ||
                      table[(row1 - 2)][(col1 - 2)] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 - 3)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 - 3;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูบนขวา
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][col1] == '♙' ||
                      table[(row1 - 2)][col1] ==
                          '♕') &&
                  table[(row1 - 3)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 - 3;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างขวา
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][(col1 - 2)] == '♙' ||
                      table[row1][(col1 - 2)] == '♕') &&
                  table[(row1 + 1)][(col1 - 3)] == ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.lcolkign = col1 - 3;
                this.lrowking = row1 + 1;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างซ้าย
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][col1] == '♙' ||
                      table[row1][col1] == '♕') &&
                  table[(row1 + 1)][(col1 + 1)] ==
                      ' ') { // ตรวจสอบหมากฝั่งสีน้ำเงิน และตรวจสอบตำแหน่งถัดจากหมากสีน้ำเงินว่าว่างไหม
                this.rcolking = col1 + 1;
                this.rrowking = row1 + 1;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }

    }else if(table[(row1 - 1)][(col1 - 1)] == '♕' ){ // ตรวจสอบตัวฮอสฝั่งสีน้ำเงิน
      // ตรวจสอบศัตรูบนซ้าย
      if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][(col1 - 2)] ==
                          '♟' ||
                      table[(row1 - 2)][(col1 - 2)] ==
                          '♛') &&
                  table[(row1 - 3)][(col1 - 3)] ==
                      ' ') {  // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.lcol = col1 - 3;
                this.lrow = row1 - 3;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูบนขวา
          if ((row1 - 2) > -1 &&
              (row1 - 2) < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 - 3) > -1 &&
                (row1 - 3) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[(row1 - 2)][col1] == '♟' ||
                      table[(row1 - 2)][col1] ==
                          '♛') &&
                  table[(row1 - 3)][(col1 + 1)] ==
                      ' ') {  // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.rcol = col1 + 1;
                this.rrow = row1 - 3;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างซ้าย
          if (row1 > -1 &&
              row1 < 8 &&
              (col1 - 2) > -1 &&
              (col1 - 2) < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 - 3) > -1 &&
                (col1 - 3) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][(col1 - 2)] == '♟' ||
                      table[row1][(col1 - 2)] == '♛') &&
                  table[(row1 + 1)][(col1 - 3)] == ' ') {  // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.lcolkign = col1 - 3;
                this.lrowking = row1 + 1;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }
          // ตรวจสอบศัตรูล่างขวา
          if (row1 > -1 &&
              row1 < 8 &&
              col1 > -1 &&
              col1 < 8) { // ตรวจสอบขอบตาราง
            if ((row1 + 1) > -1 &&
                (row1 + 1) < 8 &&
                (col1 + 1) > -1 &&
                (col1 + 1) < 8) { // ตรวจสอบขอบตาราง
              if ((table[row1][col1] == '♟' ||
                      table[row1][col1] == '♛') &&
                  table[(row1 + 1)][(col1 + 1)] ==
                      ' ') {  // ตรวจสอบหมากฝั่งสีแดง และตรวจสอบตำแหน่งถัดจากหมากสีแดงว่าว่างไหม
                this.rcolking = col1 + 1;
                this.rrowking = row1 + 1;
                board.eat = 1;
                countEatAgain++;
              }
            }
          }

    }
    if(countEatAgain > 0 ){
      countEatAgain = 0;
      return true;
    }
    return false;
  }

  playerColor(String player) {
    // เพิ่มสีตัวหมาก
    if (player == '♟' || player == '♛') {
      stdout.write('\u001b[31;1m'); // สีแดง
    } else if (player == '♙' || player == '♕') {
      stdout.write('\u001b[34m'); // สีน้ำเงิน
    }
  }

  resetColor() {
    stdout.write('\u001b[0m');
  }

  setKing() {
    // เปลี่ยนตัวหมากธรรมดาให้กลายเป็นฮอส
    var table = board.getTable();
    for (int i = 0; i < 8; i++) {
      if (table[0][i] == '♟') {
        table[0][i] = '♛';
      }
      if (table[7][i] == '♙') {
        table[7][i] = '♕';
      }
    }
  }

  checkWin(){
    int Blue = 0;
    int Red = 0;
    var table = board.getTable();
    for (int r = 0; r < table.length; r++) {
      for (int c = 0; c < table[r].length; c++) {
        if(table[r][c] == '♟' || table[r][c] == '♛' ){
          Red++;
        }else if(table[r][c] == '♙' || table[r][c] == '♕' ){
          Blue++;
        }
      }
    }
    if(Blue == 0){
      print("Red Win");
      R.Win();
      sleep(const Duration(seconds: 1));
      return true;
    }else if(Blue == 0 ){
      print("Blue Win");
      B.Win();
      sleep(const Duration(seconds: 1));
      return true;
    }
    return false;
  }


  ShowScore(){
    print("Welcome to the game of checkers");
    print("");
    print(R.toString());
    print(B.toString());
    print("");
  }
}
