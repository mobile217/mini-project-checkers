import 'dart:io';

import 'Player.dart';

class Board{
  late Player R;
  late Player B;
  var currentPlayer = '♟' ;
  var eat = 0;
  var table = [
    [' ', '♙', ' ', '♙', ' ', '♙', ' ', '♙'],
    ['♙', ' ', '♙', ' ', '♙', ' ', '♙', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '♟', ' ', '♟', ' ', '♟', ' ', '♟'],
    ['♟', ' ', '♟', ' ', '♟', ' ', '♟', ' ']
  ];

  Board(Player R, Player B) {
    this.R = R;
    this.B = B;
  }
  
  getTable(){
    return table;
  }

  setTable(row1, col1, row2, col2){ // การเดินตัวหมาก
    var eat = table[(row1 - 1)][(col1 - 1)];
    table[(row1 - 1)][(col1 - 1)] = ' ';
    table[(row2 - 1)][(col2 - 1)] = eat;
    int r1 = row1 - 1;
    int r2 = row2 - 1;
    int c1 = col1 - 1;
    int c2 = col2 - 1;
    double e1 = (((r1-r2)/2)+r2);
    double e2 = (((c1-c2)/2)+c2);
    eatPalyer(this.eat, e1.toInt(), e2.toInt());
  }

  eatPalyer(int eat ,int e1, int e2){ // ทำการกินหมากตรงข้าม
    if(eat == 1){

      table[e1][e2] = ' '; // ทำตัวหมากที่ถูกกินให้เป็นช่องว่าง
    }
    this.eat = 0; 
  }

  switchPlayer() { // สลับผู้เล่น
    if (currentPlayer == '♟') {
      currentPlayer = '♙';
    } else {
      currentPlayer = '♟';
    }
  }
}